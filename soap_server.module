<?php
/**
 * @file
 *  Enable SOAP for services module (6.x-3.x).
 *  
 *  The XMLRPC style {resource}.{method} method names have been replaced with {resource}_{method}
 *  which are valid PHP function names.
 *  
 *  The WSDL is very generic allowing any request and response objects.
 */

/**
 * Implementation of hook_server_info().
 * 
 * This function tells services module that we are providing a server
 */
function soap_server_server_info() {
  return array(
    'name' => 'SOAP',
  );
}

/**
 * Implementation of hook_server().
 * 
 * The services endpoint callback function that handles all requests to a soap server 3 endpoint
 * If ?wsdl is appended to the URL, the WSDL is served
 */
function soap_server_server() {
  $endpoint =  services_server_info_object();
  $get = $_GET;
  // serve the WSDL if ?wsdl is appended to the URL
  if (in_array('wsdl', $get)) {
    soap_server_wsdl_output($endpoint);
    // soap_server_wsdl_output() delivers the wsdl and exits 
  }
  ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache
  $wsdl_url = url($endpoint->endpoint_path, array('absolute' => TRUE, )). '?wsdl';
  try {
    $server = new SoapServer($wsdl_url);
   	$server->setClass(ServicesSoapServer);
    $server->handle();   
  } 
  catch (Exception $e) {
    watchdog('soap_server', $e->getMessage(), 'error');
  }
  exit;
}

/**
 * Implementation of hook_menu().
 * 
 */
function soap_server_menu() {
  /*
  $items['soap_server/wsdl/%endpoint'] = array(
    'type' => MENU_CALLBACK,
    'title' => 'Soap Server 3 WSDL',
    'page callback' => 'soap_server_wsdl_output',
    'page arguments' => array(2),
    'access arguments' => array('access soap server'),
  );
  */
  $items['soap_server/debug_wsdl/%endpoint'] = array(
    'type' => MENU_CALLBACK,
    'title' => 'Soap Server 3 Debug WSDL',
    'page callback' => 'soap_server_debug_wsdl',
    'page arguments' => array(2),
    'access arguments' => array('debug soap server'),
  );
    $items['soap_server/debug_client/%endpoint/%node'] = array(
    'title'            => 'Soap Server Debug Client',
    'access arguments' => array('debug soap server'),
    'page callback'    => 'soap_server_debug_client',
    'page arguments'   => array(2),
    'type'             => MENU_CALLBACK,
  );
  return $items;
}


/**
 * Implementation of hook_perm().
 */
function soap_server_perm() {
  return array('access soap server', 'debug soap server');
}

/**
 * endpoint menu loader
 * 
 * Return the services endpoint object from the endpoint name
 * 
 * @param unknown_type $endpoint_name
 */
function endpoint_load($endpoint_name) {
  $endpoint = services_endpoint_load($endpoint_name);
  if (is_object($endpoint) && $endpoint->server == "soap_server") {
    return $endpoint;
  }
  return FALSE;
}

/**
 * Get a WSDL for the given endpoint 
 * 
 * The WSDL provides a soap method for each method of the configured resources. 
 * See /admin/build/services/your_endpoint/resources
 * 
 * @param $endpoint
 */
function soap_server_get_wsdl($endpoint) {  
  $service_endpoint = url($endpoint->path, array('absolute' => TRUE));
  // get the content of the schema for each hose_xml profile using the standard namespace xs:
  $methods = _soap_server_get_methods($endpoint->name);
  foreach ($methods as $method_name => $method_config) {
    // requests can specify which field to use to identify the node - default is nid
    $requests .= "
  <message name='". $method_name ."_request'>
  	<part name='request_object' type='xsd:any'/>
  </message>";
    $responses .= "
  <message name='". $method_name ."_response'>
  	<part name='response_object' type='xsd:any'/>
  </message>";
    $port_type_operations .= "
  <operation name='$method_name'>
    <input message='tns:". $method_name ."_request'/>
    <output message='tns:". $method_name ."_response'/>
  </operation>";
    $binding_operations .= "
  <operation name='$method_name'>
    <soap:operation soapAction='urn:xmethods-delayed-quotes#$method_name'/>
    <input>
      <soap:body use='encoded' namespace='urn:xmethods-delayed-quotes'
        encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'/>
    </input>
    <output>
      <soap:body use='encoded' namespace='urn:xmethods-delayed-quotes'
        encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'/>
    </output>
  </operation>  
";
  }
  $include = drupal_get_path('module', 'soap_server').'/wsdl/soap_server.wsdl.inc';
  if (!is_file($include)) {
    return t("Could not load include @inc", array('@inc' => $include));
  }
  else {
    require_once($include);
    // $wsdl_content is assigned in the include file
    return $wsdl_content;
  }
}

/**
 * Delivers XML suitable for supplying WSDL to Soap clients
 * @param $xml - The content of the WSDL to serve
 */
function soap_server_wsdl_output($endpoint) {
  $wsdl_content = soap_server_get_wsdl($endpoint);
  ob_end_clean();
  drupal_set_header('Connection: close');
  drupal_set_header('Content-Length: '. strlen($wsdl_content));
  drupal_set_header('Content-Type: application/wsdl+xml; charset=utf-8');
  drupal_set_header('Date: '. date('r'));
  echo $wsdl_content;
  exit;
}

/**
 * Display the content of the WSDL for debugging
 * @param $endpoint
 */
function soap_server_debug_wsdl($endpoint) {
  $wsdl_content = soap_server_get_wsdl($endpoint);
  if (module_exists('geshifilter')) {
  	$geshi_inc = drupal_get_path('module', 'geshifilter') .'/geshifilter.pages.inc';
  	require_once $geshi_inc;
  	$wsdl_content = geshifilter_geshi_process($wsdl_content, 'xml', TRUE);

  }
  else {
  	$wsdl_content = "<code>". htmlspecialchars($wsdl_content) ."</code>";
  }
  return $wsdl_content ;
}

/**
 * 
 * Soap Server 3 Class for handling soap requests
 *
 */
class ServicesSoapServer { 
  public function __call($methodName, $args) { 
    // handle the request
    $services_method_name = str_replace('_', '.', $methodName);
    $endpoint = services_get_server_info('endpoint', '');
    $controller = services_controller_get($services_method_name, $endpoint);
    try {
      $ret = services_controller_execute($controller, $args);
    } 
    catch (Exception $e) {
      $code = $e->getCode();
      $soap_fault = new SoapFault($e->getMessage(), $code);
      watchdog('soap_server', $e->getMessage(), 'error');
      throw $soap_fault;
    }
    return $ret;
  } 
} 

/**
 * Return a list of service methods for the endopint
 * Enter description here ...
 * @param unknown_type $endpoint
 */
function _soap_server_get_methods($endpoint) {
  $resources = services_get_resources($endpoint);
  foreach ($resources as $resource => $methods) {
    foreach ($methods as $method_name => $method_data) {
     // we are translating the method names to avoid problems with XMLRPC's resource.method notation in PHP 5
     $hose_method_name = $resource .'_'. $method_name;
     $supported_methods[$hose_method_name] = $method_data['callback'];
    }
  }
  return $supported_methods;
}

/**
 * Debug function for soap_server services
 * You need devel module for this to work
 * @param unknown_type $nid 
 */
function soap_server_debug_client($endpoint, $nid) {
  if (!module_exists('devel')) {
    drupal_set_message(t('Devel module is required for the debug client function.'), 'error');
    return t("fail");
  }
  ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache
  $wsdl_url = url($endpoint->path. '?wsdl', array('absolute' => TRUE));
  try {
    $client = new SoapClient($wsdl_url, array(
    	'trace' => 1
    ));
    // show the available functions as specified in the WSDL
    $functions = $client->__getFunctions();
    dsm(array('client functions' => $functions));
    // retrieve a node
    $return = $client->node_retrieve($nid);  
    $response = $client->__getLastResponse();
    $request = $client->__getLastRequest();
    dsm(array('request' => $request));
    dsm(array('response' => $response));
    dsm(array('return' => $return));
    // retrieve a user
    $return = $client->user_retrieve(1);  // anonymous user needs "access user profiles" perm
    $response = $client->__getLastResponse();
    $request = $client->__getLastRequest();
    dsm(array('request' => $request));
    dsm(array('response' => $response));
    dsm(array('return' => $return));
  } catch (Exception $e) {
    dsm(array('client' => $e));
    return t("fail");
  }
  return t("success");    	
}
